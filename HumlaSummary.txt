Advanced Google search can also be performed using the page http://www.google.com/advanced_search, which allows us to perform restricted search without using the operators mentioned above. 

The Google Hacking Database created by Johnny Long can be found at http://www.hackersforcharity.org/ghdb/ though it is not updated, yet it is a great place to understand and learn how we can use Google to find out sensitive information. A regularly updated version can be found at http://www.exploit-db.com/google-dorks/. 

robtex.com
yougetsignal.com > Reverse IP Search 
viewdns.info > gain DNS information

Bing						
	ip: This unique operator provided by Bing allows us to search web pages based upon IP address. Using it we can perform a reverse IP search, which means it allows us to look for pages hosted on the specified IP). Example: ip:176.65.66.66 

	feed:			
	Yet another unique operator provided by Bing is feed, which allows us to look for web feed pages containing the provided keyword.
							
	One other feature that Bing provides is to perform social search using the page https://www.bing.com/explore/social. It allows us to connect our social network accounts with Bing and perform search within them. 

Yahoo
						
	link: It is another interesting operator which allows us to lookup web pages which link to the specific web page provided. While using this operator do keep in mind to provide the URL with the protocol (http:// or https://). 
	link: http://null.co.in				

	So these are the operators which Yahoo supports. Apart from these we can access the Yahoo advanced search page at http://search.yahoo.com/search/options?fr= fp-top&p=, which allows us to achieve well-filtered search results. One other thing that Yahoo offers is advanced news search which can be performed using the page http://news.search.yahoo.com/advanced . 
					

Yandex

	url:
	This “url” search query parameter is also an add-on. It searches for the exact URL provided by the user in Yandex database.
							
	Example: url:http://attacker.in
	Here Yandex will provide a result if and only if the URL has been crawled and indexed in its database. 
							 	 	 		
	host:
							
	It can be used to search all the available hosts. This can be used by the penetration testers mostly.
							
	Example: host:owasp.org
							
	rhost:
							
	It is quite similar to host but “rhost” searches for reverse hosts. This can also be used by the penetration testers to get all the reverse host details.

	title:(mad tea-party)
	date:200712*
							
	It can be used in two ways. One is for subdomains by using the wildcard operator * at the end or another without that.
							
	Example: rhost:org.owasp.* rhost:org.owasp.www 
						
	site:
							
	This operator is like the best friend of a penetration tester or hacker. This is avail- able in most of the search engines. It provides all the details of subdomains of the provided URL.
							
	For penetration testers or hackers finding the right place to search for vulner- ability is most important. As in most cases the main sites are much secured as compared to the subdomains, if any operator helps to simplify the process by providing details of the subdomains to any hacker or penetration tester then half work is done. So the importance of this operator is definitely felt in security industry.
							
	Example: site:http://www.owasp.org
	It will provide all the available subdomains of the domain owasp.com as well as

	Advanced Search: http://www.yandex.com/search/advanced?&lr=10558 


Other Important Search Engines
	NerdyData - Client Side source code search Engine
	pipl - People Search Engine
	WolfRamAlpha - Advanced Data 
	SHODAN - Device search engine for hackers
	censys - Same as shodan, pros and cons are there. Explore.
	ZoomEye - Same as Shodan. Bit cheaper APIs.
	PunkSpider - Scans website regularly. Check for already available vulnerabilities. Ex. race360.com
	Archive Wayback Machine - Archive Search Engine.
	wigle.net - network Search Engine
	imageforensics.org - Image Forensic Search Engine 
	TinyEYE - Reverse Image Search
	Google Image Search - Reverse Image Search


Small Challenge:
What is the version of Sharepoint running on an American server with following details:
ServerName: CEI02, Instance Name: Sharepoint. 


Pastebin Sample Search:
http://pastebin.com/search?cx=013305635491195529773%3A0ufpuq-fpt0&cof=FORID%3A10&ie=UTF-8&q=dump+database&sa.x=0&sa.y=0&sa=Searchhttp://www.corelan.be/index.php/2011/03/22/pastenum-pastebinpastie-enumeration-tool/


GitHub Search Utility

trueCaller - Check for Name / Image for specified Mobile Number.


Social Media Intelligence

Monitoring Twitter for specific Tweets (Send an alert on mail / Dump Data in ElasticSearch)
Use tweepystreaming.py for the same. Uploaded in the same folder. 
Or download latest version from github.com/upgoingstar/TwitterBot/


FaceBook Graph Search UI
http://graph.tips/
https://inteltechniques.com/intel/OSINT/facebook.html

Sample FB Graph Search: 
“Single women named ‘Rachel’ from Los Angeles, California who like football and Game of Thrones and live in the United States.” 

DarkNet Search Engine
http://thehiddenwiki.org/

Linkeding Advanced Search:
https://www.LinkedIn.com/vsearch/p?trk=advsrch&adv=true

((Pentester OR “Security Analyst” OR “Consultant” OR “Security Consultant” OR “Information Security Engineer”) AND (Analyst OR “Security Engineer” OR “Network Security Engineer”)) NOT Manager.
					


Maltego	
			
Maltego is an Open Source Intelligence application, which provides a platform to not only extract data but also to represent that data in a format which is easy to understand as well as analyze.

	Entity: An entity is a piece of data which is taken as an input to extract further information. E.g. domain name xyz.com

	Transform: A piece of code which takes an entity (or a group of entities) as an input and extracts data in the form of entity (or entities) based upon the relationship. E.g. DomainToDNSNameSchema: this transform will try to test various name schemas against a domain (entity).
	
	Machine: A machine is basically a set of transforms linked programmatically. E.g. Footprint L1: a transform which takes a domain as an input and generates various types of information related to the organization such as emails, AS number etc.

	Domain to subdomain
		null.co.in → to website dns → select all → to server technology


	Domain to ip address
		google dns from domain → to website dns → select all → resolve to ip → to ip address dns


	DOMAIN TO E-MAIL ADDRESS
						
		There is a set of transforms for extracting e-mail address directly from a domain, but for this example we will be following a different approach using metadata. Let’s again take a domain entity and run all the transforms in the set “Files and Documents from Domain.” As the name itself says, it will look for files listed in search engine for the domain. Once we get a bunch of files, we can select them and run the transform “Parse meta information.” It will extract the metadata from the listed files. Now let’s run all the transforms in the set “Email addresses from person” on the entities of type entity and provide the appropriate domain (domain we are looking for in the e-mail address) and a blank for additional terms. We can see the result from this final trans- form and compare it with the result of running the transform set for e-mail extraction running directly on the domain and see how the results are different.
								
		E.g.: Domain = paterva.com 


	PERSON TO WEBSITE
						
		For this example we will be using a machine “Person - Email address.” Let’s take an entity of type person and assign it a value “Andrew MacPherson” and run the machine on this entity. The machine will start to enumerate associated e-mail IDs using different transforms. Once it has completed running one set of transforms it will provide us the option to move forward with selected entities, enumerated till now. From the above example we know “andrew@punks.co.za” is a valid e-mail address so we will go ahead with this specific entity only. What we get as an end result is websites where this specific e-mail address occurs, by running the trans- form “To Website [using Search Engine]” (as a part of the machine). 

HaveIBeenPwned? - Checks for your email id has got compromised in any of the big database hacks. 
https://haveibeenpwned.com/


maltego Transform by SudhanshuC
https://github.com/SudhanshuC/Maltego-Transforms


Use in Python
python emailhibp.py google@gmail.com


Use in Maltego:
	Download maltego transform library first
	put it where hibp gonna be
	Downloal hibp

	We can see that the response is a XML styled output and contains the string “Pwned at [“Adobe”,“Gawker”,“Stratfor”]”. This means our code is working prop- erly and we can use this as a transform. Maltego takes this XML result and parses it to create an output. Now our next step is to configure this as a transform in Maltego.
						
	Under the manage tab go to Local Transform button to start the Local Transform Setup Wizard. This wizard will help us to configure our transform and include it into our Maltego instance. 

			 	 	 
	Now click on next and move to the second phase of the wizard. Here under the command field we need to provide the path to the programming environment we are going to use to run the transform code. In our case it would be
							
	 /usr/bin/python (for Linux)


							
	C:\Python27/python.exe (for Windows)
							
	Once the environment is set we can move to the parameters field, here we will provide the path to our transform script. For example,
							
	  /root/Desktop/transforms/emailhibp.py (for Linux)
							
	C:\Python27\transforms\emailhibp.py (for Windows)
							
	One point to keep in mind here is that if we select the transform file using the browse button provided in front of the “Parameters” field, then it will simply take the file name in the field, but we need absolute path of the transform to execute it so provide the path accordingly.  Note down the unique name that is assigned.
						
	MAchine
	domain to hibp
	start to crate a timer machine


Recon-ng - Framework for Open Source Intelligence

	Discovery (Active recon with sending packet)
	Exploitation (Using payload)
	Import (to add list or prev projs)
	Recon (passive recon)
	Report (xml or html)


	Commands in order of how we used them in Humla
		help
		Workspaces
		Workspaces list                        	to get the lists
		Workspaces add osint
		
		Keys list - to see which keys has been added
			https://bitbucket.org/LaNMaSteR53/recon-ng/wiki/Usage%20Guide#!acquiring-api-keys
		
			Add bing key     fVGoRoqI5ZHSle5ZM0B3o0LSAsINFZ+l9AkA2gFiF4s
		
		Show Modules 	(Take a domain and dig deeper)
			recon/domains-hosts/bing_domain_api(to get whole bunch of hosts from domain)
		
		Show info
			set SOURCE fbi.gov
		
		Run
			recon/domains-hosts/bing_domain_web
			use recon/domains-hosts/netcraft	(to get more hosts) http://toolbar.netcraft.com/site_report
			Show dashboard - to see what we did so far
			Show hosts - host table
		
		Lets fill the table with ips first
			use recon/hosts-hosts/resolve
			use recon/hosts-hosts/bing_ip

		Lets look for some technology information bug bounty $$$
			Use recon/domains-hosts/builtwith  to get technology idea
			recon/domains-vulnerabilities/punkspider   to get free bugs
		
		Show in site http://punkspider.hyperiongray.com/   race360
		
		Lets get some contact details
			Use recon/domains-contacts/whois_pocs
			Show contacts
			use recon/domains-contacts/pgp_search
			
		Harvest info from a perticular place about our target
			Use recon/profiles-profiles/namechk 	  makash :P
		
		Get credentials
			use recon/contacts-credentials/hibp_paste  for google@gmail.com



Other Useful Stuff

